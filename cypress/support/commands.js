// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

Cypress.Commands.add('validateErrorMessage', ()=>{
    cy.contains('Logi sisse').should('exist')
    cy.get('.error-message').should('exist')
})

Cypress.Commands.add('inputValidation', (selector)=> {
    cy.get(selector)
        .invoke('prop', 'validationMessage')
        .should('equal', 'Please fill out this field.')

    cy.get(selector)
        .clear()
        .type('123')
        .invoke('prop', 'validationMessage')
        .should('not.equal', 'Please fill out this field.')
})

Cypress.Commands.add('login', (email, password) => {
    cy.get('#login-input').clear().type(email)
    cy.get('#password-input').clear().type(password)
    cy.get('.wrap').then($wrap => {
        if($wrap.find('[style="width: 304px; height: 78px;"] > div > iframe').length > 0){
            cy.log('found captcha')
            cy.get('[style="width: 304px; height: 78px;"] > div > iframe')
                .then($iframe => {
                    const $body = $iframe.contents().find('body');
                    cy.wrap($body)
                        .find('.recaptcha-checkbox-border')
                        .should('be.visible')
                        .click();
                });
        }
        else {
            cy.log('captcha not found')
        }
    })
    cy.wait(2000)
    cy.get('input[type="submit"]').click()
})




