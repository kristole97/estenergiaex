

describe('check login functionality', ()=>{

    before(()=>{
        cy.visit('/login/')
    })

    it('should display validation message on empty username field', function () {
        cy.inputValidation('#login-input')
    });

    it('should display validation message on empty password field', function () {
        cy.inputValidation('#password-input')
    });

    it('should not login with incorrect credentials', function () {
        cy.login('a','b')
        cy.validateErrorMessage()
    });

    it('should not login with correct username and incorrect password', function () {
        cy.login('somekindofusername','b')
        cy.validateErrorMessage()
    });

    it('should not login with incorrect username and correct password', function () {
        cy.login('a','foobar15')
        cy.validateErrorMessage()
    });

    it('should login with correct credentials', function () {
        cy.login('somekindofusername','foobar15')
        cy.contains('Logi sisse').should('not.exist')
        cy.get('.menuopener', {timeout: 6000}).trigger('mouseover')
        cy.get('a[href="/logout/"]').click()
        cy.contains('Logi sisse').should('exist')
    });

})